import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function newPost(props) {
  const state = {
    title: "",
    content: "",
  };

  const titleChange = (e) => {
    state.title = e.target.value;
  };
  const contentChange = (e) => {
    state.content = e.target.value;
  };
  function save() {
    props.savePost(state);
  }

  function publish() {
    props.publish(props.paramID);
  }

  return (
    <>
      <Container className="w-25 my-5">
        <Row className="mt-2 mb-3">
          <Col>
            <h1>New Post</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  name="title"
                  type="text"
                  placeholder="Title"
                  onChange={titleChange}
                />
              </Form.Group>

              <Form.Group
                className="mb-3"
                controlId="formBasicPassword"
                onChange={contentChange}
              >
                <Form.Label>Content</Form.Label>
                <Form.Control
                  name="content"
                  as="textarea"
                  placeholder="Content"
                />
              </Form.Group>
              <Row className="mb-2">
                <Col>
                  <Button className="w-100" variant="primary" onClick={save}>
                    Save
                  </Button>
                </Col>
                <Col>
                  <Button href="/post" className="w-100" variant="primary">
                    Cancel
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Button className="w-100" variant="primary" onClick={publish}>
                    Publish Now
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default newPost;
