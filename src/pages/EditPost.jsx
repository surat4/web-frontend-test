/* eslint-disable react-hooks/rules-of-hooks */
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useParams } from "react-router-dom";
import axios from "axios";
import { useState, useEffect } from "react";

function editPost(props) {
  const param = useParams();

  let [dataState, setDataState] = useState({
    title: "",
    content: "",
  });
  const [editId, setId] = useState("");

  function getDataId() {
    axios
      .get(`http://dev.opensource-technology.com:3000/api/posts/${param.id}`)
      .then((res) => {
        setId(param.id);
        setDataState(res.data);
      });
  }

  // eslint-disable-next-line react-hooks/rules-of-hooks
  useEffect(() => {
    getDataId();
  }, []);

  const titleChange = (e) => {
    setDataState((data) => ({
      ...data,
      ...{ title: e.target.value },
    }));
  };
  const contentChange = (e) => {
    setDataState((data) => ({
      ...data,
      ...{ content: e.target.value },
    }));
  };
  const save = () => {
    props.handleEdit(editId, dataState);
  };

  const Delete = () => {
    props.deletePost(editId);
  };

  return (
    <>
      <Container className="w-25 my-5">
        <Row className="mt-2 mb-3">
          <Col>
            <h1>Edit Post</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  name="title"
                  type="text"
                  placeholder="Title"
                  value={dataState.title}
                  onChange={titleChange}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Content</Form.Label>
                <Form.Control
                  name="content"
                  as="textarea"
                  placeholder="Content"
                  value={dataState.content}
                  onChange={contentChange}
                />
              </Form.Group>
              <Row className="mb-2">
                <Col>
                  <Button className="w-100" variant="primary" onClick={save}>
                    Save
                  </Button>
                </Col>
                <Col>
                  <Button href="/post" className="w-100" variant="primary">
                    Cancel
                  </Button>
                </Col>
                <Col>
                  <Button className="w-100" variant="primary" onClick={Delete}>
                    Delete
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default editPost;
