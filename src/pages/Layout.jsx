import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { Outlet } from "react-router-dom";

function layout() {

    return (
        <>
            <div className='d-flex justify-content-center'>
                <Container style={{margin: '3rem 4rem'}}>
                    <Row className='mb-5'>
                        <Col md={10}>
                            <Button href="/post" style={{marginRight: '2rem', width: '7rem'}} size="lg"variant="primary">Post</Button>
                            <Button href="/draft" style={{width: '7rem'}}variant="primary" size="lg">Draft</Button>
                        </Col>
                        <Col md={2}>
                            <Button href="/new" variant="dark" size="lg">Create Draft</Button>
                        </Col>
                    </Row>
                </Container>
            </div>
            <Outlet />
        </>
      )
}

export default layout