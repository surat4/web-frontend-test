import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

function draft(props) {
  const data = props.data;

  const Edit = (id) => {
    props.edit(id);
  };
  const Published = (id) => {
    props.publish(id);
  };
  const Delete = (id) => {
    props.deletePost(id);
  };
  return (
    <>
      {data.map((x) => (
        // eslint-disable-next-line react/jsx-key
        <Container className="border border-dark rounded w-75 mb-2">
          <Row className="mt-2 mb-3">
            <Col>
              <h1>{x.title}</h1>
            </Col>
          </Row>
          <Row className="mb-3">
            <Col>
              <span>{x.content}</span>
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md={8}>
              <span>{x.created_at}</span>
            </Col>
            <Col className="d-flex justify-content-end">
              <Button
                style={{ marginRight: "7px" }}
                onClick={() => {
                  Edit(x.id);
                }}
              >
                Edit
              </Button>
              <Button
                style={{ marginRight: "7px" }}
                onClick={() => {
                  Published(x.id);
                }}
              >
                Published
              </Button>
              <Button
                onClick={() => {
                  Delete(x.id);
                }}
              >
                Delete
              </Button>
            </Col>
          </Row>
        </Container>
      ))}
    </>
  );
}

export default draft;
