// import { useState } from 'react';
import useFetch from "./hooks/useFetch";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Post from "./pages/Post";
import Draft from "./pages/Draft";
import NewPost from "./pages/NewPost";
import EditPost from "./pages/EditPost";
import Layout from "./pages/Layout";

const baseUrl = "http://dev.opensource-technology.com:3000/api";

function App() {
  const {
    posts,
    drafts,
    paramID,
    savePost,
    publish,
    deletePost,
    handleEdit,
    getId,
  } = useFetch(baseUrl);

  if (!posts) return null;
  if (!drafts) return null;
  // ------------------------------------- หน้าจอที่แสดง -------------------------------------------
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route
              path="post"
              element={<Post data={posts} edit={getId} publish={publish} />}
            />
            <Route
              path="draft"
              element={
                <Draft
                  data={drafts}
                  edit={getId}
                  publish={publish}
                  deletePost={deletePost}
                />
              }
            />
          </Route>
          <Route
            path="new"
            element={
              <NewPost
                paramID={paramID}
                savePost={savePost}
                publish={publish}
              />
            }
          />
          <Route
            path="edit/:id"
            element={
              <EditPost handleEdit={handleEdit} deletePost={deletePost} />
            }
          />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
