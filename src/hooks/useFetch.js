import axios from "axios";
import { useState, useEffect } from "react";

function useFetch(url) {
  const [posts, setPosts] = useState([]);
  const [drafts, setDrafts] = useState([]);
  const [paramID, setId] = useState([]);

  async function getPost() {
    await axios.get(`${url}/posts?limit=20`).then((res) => {
      setPosts(res.data.posts);
    });
  }
  
  async function getDraft() {
    await axios.get(`${url}/posts/draft?limit=20`).then((res) => {
      setDrafts(res.data.posts);
    });
  }

  function getId(id) {
    location.href = `/edit/${id}`;
  }

  function savePost(state) {
    const bodyForm = new FormData();
    bodyForm.append("title", state.title);
    bodyForm.append("content", state.content);
    axios.post(`${url}/posts`, bodyForm).then((res) => {
      console.log(res);
      setId(res.data.id)
      alert("create success");
    });
  }

  function publish(id) {
    const bodyForm = new FormData();
    bodyForm.append("published", true);
    axios.patch(`${url}/posts/${id ? id : paramID}`, bodyForm).then((res) => {
      console.log(res.data);
      alert("published success");
      location.href = "/post";
    });
  }

  const handleEdit = (id, state) => {
    const bodyForm = new FormData();
    bodyForm.append("title", state.title);
    bodyForm.append("content", state.content);
    bodyForm.append("published", true);
    axios.patch(`${url}/posts/${id}`, bodyForm).then((res) => {
      console.log(res);
      alert(`update success`);
      location.href = "/post";
    });
  };
  function deletePost(id) {
    axios.delete(`${url}/posts/${id}`).then((res) => {
      console.log(res);
      alert(`delete id: ${id}`);
      location.href = "/post";
    });
  }
  useEffect(() => {
    getPost();
    getDraft();
  }, []);

  return { posts, drafts, paramID, savePost, publish, deletePost, handleEdit, getId };
}

export default useFetch;
