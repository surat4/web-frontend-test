# web-frontend-test

## Getting started

Install Node js (Recommended For Most Users)
https://nodejs.org/en

Install Visual Code + Extensions
https://code.visualstudio.com/

- ES7+ React/Redux/React-Native snippets
- ESLint
- Color Hightlight(optional)
- Code Snapshot(optional)
- Auto Rename Tag(optional)
- Prettier - Code Formatter

Open Command Prompt

```
git clone https://gitlab.com/surat4/imed-nobel-project.git
cd imed-nobel-project
npm install
npm run dev
```

## Code ประกอบไปด้วย

Path API ที่ใช้ดึงข้อมูล

```
const baseUrl = 'http://dev.opensource-technology.com:3000/api'
```

มีการสร้างไฟล์ useFetch.js ใช้เก็บ rest api ในการเรียกใช้งาน
โดยเรียกใช้ useFetch ส่งbaseurl เพื่อดึงข้อมูลต่างๆ ดังนี้

- get /posts ดึงข้อมูลpost
- get /posts/draft ดึงข้อมูลdraft
- get /posts/:id ดึงข้อมูลpostตามไอดีที่ระบุ
- post /posts สร้างลิสข้อมูลdraft
- delete /posts/:id ลบข้อมูลตามไอดีที่ระบุ
- patch /posts/:id ใช้โอนย้ายข้อมูลจากข้อมูลฝัง draft ไปยังข้อมูลฝัง post

ไฟล์ App.jsx

```
const {
    posts,
    drafts,
    paramID,
    savePost,
    publish,
    deletePost,
    handleEdit,
    getId,
  } = useFetch(baseUrl);
```

ไฟล์ useFetch.js

```
function useFetch(url) {
    const [posts, setPosts] = useState([]);
    const [drafts, setDrafts] = useState([]);
    // const [dataState, setDataState] = useState({
    //     title: '',
    //     content: '',
    // });
    // const [editId, setId] = useState([]);

    async function getPost() {
        await axios.get(`${url}/posts?limit=20`).then((res) => {
            setPosts(res.data.posts);
        });
    }
    async function getDraft() {
        await axios.get(`${url}/posts/draft?limit=20`).then((res) => {
            setDrafts(res.data.posts);
        });
    }
    function getId(id) {
        location.href = `/edit/${id}`
    }
    // function getDataId(id) {
    //     axios.get(`${url}/posts/${id}`).then((res) => {
    //         setId(id)
    //         setDataState(res.data)
    //         alert(`getData id: ${id}`)
    //         location.href = `/edit/${id}`
    //     })
    // }

    function savePost(state) {
      const bodyForm = new FormData();
      bodyForm.append('title', state.title)
      bodyForm.append('content', state.content)
      axios.post(`${url}/posts`, bodyForm).then((res) => {
        console.log(res)
        alert('create success')
        location.href = '/post'
      })
    }

    function publish(id) {
        const bodyForm = new FormData();
        bodyForm.append('published', true)
        axios.patch(`${url}/posts/${id}`, bodyForm).then((res) => {
            console.log(res.data)
            alert('published success')
        })
    }

    const handleEdit = (id, state) => {
      const bodyForm = new FormData();
      bodyForm.append('title', state.title)
      bodyForm.append('content', state.content)
      bodyForm.append('published', true)
      axios.patch(`${url}/posts/${id}`, bodyForm).then((res) => {
        console.log(res)
        alert(`update success`)
        location.href = '/post'
      })
    }
    function deletePost(id) {
        axios.delete(`${url}/posts/${id}`).then((res) => {
            console.log(res)
            alert(`delete id: ${id}`)
            location.href = '/post'
        })
    }
    useEffect(() => {
        getPost();
        getDraft();
    }, [])

    return { posts, drafts, savePost, publish, deletePost, handleEdit, getId }
}
```

เงื่อนไขกรณีไม่มีข้อมูลส่งมาให้ค่าเป็น null

```
if (!posts) return null;
if (!drafts) return null;
```

ส่วนนี้จะเป็นการ route โดยใช้ react-router-dom
จะมีหน้าจอ 5 ส่วน

- Layout path="/" ส่วนโครงสร้างปุ่ม post draft และปุ่ม create draft
- Post path="/post" ส่วนแสดงข้อมูลฝัง post มีการ props ค่า data edit
- Draft path="/draft" ส่วนแสดงข้อมูลฝัง draft มีการ props ค่า data edit publish deletePost
- NewPost path="/new" ส่วนแสดงข้อมูลฝัง draft มีการ props ค่า paramID publish savePost
- EditPost path="/edit/:id" ส่วนแสดงข้อมูลฝัง draft มีการ props ค่า handleEdit deletePost

```
return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route
              path="post"
              element={<Post data={posts} edit={getId} publish={publish} />}
            />
            <Route
              path="draft"
              element={
                <Draft
                  data={drafts}
                  edit={getId}
                  publish={publish}
                  deletePost={deletePost}
                />
              }
            />
          </Route>
          <Route
            path="new"
            element={
              <NewPost
                paramID={paramID}
                savePost={savePost}
                publish={publish}
              />
            }
          />
          <Route
            path="edit/:id"
            element={
              <EditPost handleEdit={handleEdit} deletePost={deletePost} />
            }
          />
        </Routes>
      </BrowserRouter>
    </>
  );
```
